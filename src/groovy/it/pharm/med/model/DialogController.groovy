package it.pharm.med.model

class DialogController {

	def afterInterceptor = { model, modelAndView ->
		if ( params['dialog'] ) {
			model.layout = 'dialog';
			model.extraparams = [ 'dialog': 'true' ] ;
		} else {
			model.layout = 'main';
			model.extraparams = [];
		}
	}
	
	def extraparams() {
		
		def extraparams = new LinkedHashMap();
		
		if( params[ 'dialog' ] )
		  extraparams[ 'dialog' ] = params[ 'dialog' ]
		
		return extraparams
		
	}
	
}
