package it.pharm.med.file

import it.pharm.med.model.Molecule

class MatchFile {

	File file

	MatchFile(file) {
		this.file = file
	}

	def parse() {

		if(!file.exists())
			return null
			
		def moleculeInfo = []
		def matchInfo = new MatchInfo()
		def found	
		
		file.eachLine { line ->
			
			if(line.trim()) {

				if(found){
					matchInfo.rmsd = line
					found = false
					
					try{
					
						File f = new File(matchInfo.filenamePdb)
						
						def molecule = Molecule.findByFileNamePdb(f.name)
						if(molecule) {
							matchInfo.filenamePdb = f.name
							matchInfo.moleculeName = molecule.name
							matchInfo.link = molecule.id
						}
					
					}catch(Exception e){
						matchInfo.moleculeName = e.message
					}

					moleculeInfo.add matchInfo
					matchInfo = new MatchInfo()
				
				}else{
					if(line.indexOf(".pdb")!=-1)
						matchInfo.filenamePdb = line.trim()
					
					if(line.indexOf("rmsd")!=-1)
						found = true
				}
			}
			
		}
		
		return moleculeInfo
			
	}

}

class MatchInfo implements Serializable {
	String link
	String moleculeName
	String filenamePdb
	String rmsd	
}