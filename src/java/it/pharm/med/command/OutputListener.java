package it.pharm.med.command;

public interface OutputListener {

	public void standardOutput(char[] output);

	public void errorOutput(char[] output);

}
