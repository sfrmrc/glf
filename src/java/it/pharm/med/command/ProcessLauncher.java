package it.pharm.med.command;

import it.pharm.med.command.exception.CommandNotExistsException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ProcessLauncher {

	public interface OutputListener {

		public void standardOutput(char[] output);

		public void errorOutput(char[] output);

	}

	private class BackgroundPrinter extends Thread {

		private InputStream in;
		boolean isErrorOutput;

		public BackgroundPrinter(InputStream in, boolean isErrorOutput) {
			this.in = in;
			this.isErrorOutput = isErrorOutput;
		}

		public void run() {
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(this.in));

				// read buffer
				char[] buf = new char[1024];

				// write data to target, until no more data is left to read
				int numberOfReadBytes;
				while ((numberOfReadBytes = reader.read(buf)) != -1) {
					char[] clearedbuf = new char[numberOfReadBytes];
					System.arraycopy(buf, 0, clearedbuf, 0, numberOfReadBytes);

					if (this.isErrorOutput) {
						fireErr(clearedbuf);
					} else {
						fireOut(clearedbuf);
					}
				}
				/*
				 * } catch (IOException ioe) { // ignore this: process has
				 * ended, causing IOException } catch (NullPointerException ioe)
				 * { // ignore this: there was no resulting output
				 */
			} catch (Exception e) {
			}
		}

		public void close() {
			try {
				this.in.close();
			} catch (Exception e) {
			}
		}

	}

	private String commandLine;
	private File baseDir;
	private Process subProcess;
	private boolean finished = false;
	private List listeners = new ArrayList(1);

	StringBuffer out = new StringBuffer();
	StringBuffer err = new StringBuffer();

	/**
	 * Constructs new process launcher with the given command line.
	 */
	public ProcessLauncher(String commandLine) {
		this(commandLine, null);
	}

	public ProcessLauncher(String commandLine, File baseDir) {
		this.commandLine = commandLine;
		this.baseDir = baseDir;
	}

	/**
	 * Launches the process, and blocks until that process completes execution.
	 * 
	 * @throws CommandNotExistsException
	 *             If the command could not be executed because it does not
	 *             exist
	 */
	public int launch() throws CommandNotExistsException, Exception {
		this.err.setLength(0);
		this.out.setLength(0);

		BackgroundPrinter stdout = null;
		BackgroundPrinter stderr = null;
		try {

			this.subProcess = Runtime.getRuntime().exec(this.commandLine, null,
					this.baseDir);

			stdout = new BackgroundPrinter(subProcess.getInputStream(), false);
			stderr = new BackgroundPrinter(subProcess.getErrorStream(), true);
			stdout.start();
			stderr.start();

			// kill process and wait max 10 seconds for output to complete
			int exitValue = this.subProcess.waitFor();
			stdout.join(10000);
			stderr.join(10000);

			return exitValue;
		} catch (IOException ioe) {
			// usually caused if the command does not exist at all
			throw new CommandNotExistsException(commandLine);
		} catch (Exception e) {
			throw e;
		} finally {
			if (this.subProcess != null) {
				this.subProcess.destroy();
				this.subProcess = null;
			}
			if (stdout != null) {
				stdout.close();
			}
			if (stderr != null) {
				stderr.close();
			}
			this.finished = true;
		}
	}

	/**
	 * Check whether execution has finished.
	 */
	public boolean hasFinished() {
		return finished;
	}

	/**
	 * Add a listener for output from the to-be-launched process.
	 */
	public void addOutputListener(OutputListener listener) {
		this.listeners.add(listener);
	}

	/** fire error output event */
	private void fireErr(char[] err) {
		if (this.listeners.isEmpty()) {
			this.err.append(out);
		}

		Iterator iter = this.listeners.iterator();
		while (iter.hasNext()) {
			((OutputListener) iter.next()).errorOutput(err);
		}
	}

	/** fire standard output event */
	private void fireOut(char[] out) {
		if (this.listeners.isEmpty()) {
			this.out.append(out);
		}

		Iterator iter = this.listeners.iterator();
		while (iter.hasNext()) {
			((OutputListener) iter.next()).standardOutput(out);
		}
	}

}
