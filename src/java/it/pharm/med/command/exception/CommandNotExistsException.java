package it.pharm.med.command.exception;

public class CommandNotExistsException extends RuntimeException {

	private static final long serialVersionUID = 1484933727653570482L;

	/**
     * Construct a new exception for a command that does not exist.
     * 
     * @param msg The message for this exception.
     */
    public CommandNotExistsException(String msg) {
      super(msg);
    }
	
}
