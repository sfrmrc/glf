// Place your Spring DSL code here
import org.codehaus.groovy.grails.commons.ConfigurationHolder

beans = {
	dbFileService(it.pharm.med.FileService) {
		baseDir = ConfigurationHolder.config.pharmer.files.dbs ?: "./dbs"
	}
	pdbFileService(it.pharm.med.FileService) {
		baseDir = ConfigurationHolder.config.pharmer.files.pdb ?: "./pdb"
	}
	sdfFileService(it.pharm.med.FileService) {
		baseDir = ConfigurationHolder.config.pharmer.files.sdf ?: "./sdf"
	}
	matchFileService(it.pharm.med.FileService) {
		baseDir = ConfigurationHolder.config.pharmer.files.match ?: "./match"
	}
}
