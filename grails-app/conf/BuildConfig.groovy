grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.dependency.resolution = {
	// inherit Grails' default dependencies
	inherits("global") {
		// specify dependency exclusions here; for example, uncomment this to disable ehcache:
		// excludes 'ehcache'
	}
	log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
	checksums true // Whether to verify checksums on resolve
	legacyResolve false

	repositories {
		inherits true // Whether to inherit repository definitions from plugins

		grailsPlugins()
		grailsHome()
		grailsCentral()

		mavenLocal()
		mavenCentral()

		mavenRepo "http://ontocat.sourceforge.net/maven/repo"

		mavenRepo "http://nexus.dbnp.org/content/repositories/releases"
		mavenRepo "http://repository.springsource.com/maven/bundles/release"
		mavenRepo "http://repository.springsource.com/maven/bundles/external"
		mavenRepo "http://repository.springsource.com/maven/libraries/release"
		mavenRepo "http://repository.springsource.com/maven/libraries/external"
	}
	dependencies {
		// specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

		runtime 'mysql:mysql-connector-java:5.1.20'

		compile("com.thoughtworks.xstream:xstream:1.3.1")
		compile "org.grails:grails-webflow:$grailsVersion"

	}

	plugins {

		provided(
			
				":tomcat:$grailsVersion",
				":grom:latest.integration",
				":console:1.2"
			
				)

		runtime (

				":fields:1.3",
				":database-migration:1.2.1",
				":resources:1.2.RC2",
				":less-resources:1.3.3.1",
				":zipped-resources:1.0"

				)

		compile (
				
				":twitter-bootstrap:2.3.0",
				":jquery:1.8.3",
				":jquery-ui:1.8.24",
				":jquery-datatables:1.7.5",
				":underscore:1.4.4",
				":hibernate:$grailsVersion",
				":famfamfam:1.0.1",
				":cache:1.0.1",
				":ajaxflow:0.2.4",
				":dbxp-module-base:0.5.0"

				)

		compile ':webflow:2.0.0', { exclude 'grails-webflow' }

	}
}

grails.server.port.http = "8585"