package it.pharm.med.conf

import it.pharm.med.model.Food
import it.pharm.med.model.FoodMolecule
import it.pharm.med.model.Group
import it.pharm.med.model.Molecule;
import it.pharm.med.model.SubGroup;

class MolecularTemplate {

	public static void init() {
		
		if (String.metaClass.getMetaMethod("grom")) "inserting initial groups".grom()
		
		def vegetables = Group.findByName("Vegetables") ?: new Group(name: "Vegetables").save()
		def meats = Group.findByName("Meats") ?: new Group(name: "Meats").save()
		
		if (String.metaClass.getMetaMethod("grom")) "inserting initial subgroups".grom()
		
		def sgroup11 = SubGroup.findByName("sg11") ?: new SubGroup(
			name: "sg1",
			group: vegetables).save()
		def sgroup21 = SubGroup.findByName("sg21") ?: new SubGroup(
			name: "sg21",
			group: meats).save()
		
		def m1sqn = Molecule.findByName("1SQN") ?:
			new Molecule(name: "1SQN", fileNamePdb: "1SQN_1biol_ligand.pdb").save()
	
		def m1upv = Molecule.findByName("1UPV") ?:
			new Molecule(name: "1UPV", fileNamePdb: "1UPV_1biol_ligand.pdb").save()
				
		def m1zdt = Molecule.findByName("1ZDT") ?:
			new Molecule(name: "1ZDT", fileNamePdb: "1ZDT_1biol_ligand.pdb").save()
				
		def m3gws = Molecule.findByName("3GWS") ?:
			new Molecule(name: "3GWS", fileNamePdb: "3GWS_5biol_ligand.pdb").save()
				
		def m3ilz = Molecule.findByName("3ILZ") ?:
			new Molecule(name: "3ILZ", fileNamePdb: "3ILZ_6biol_ligand.pdb").save()
				
		def m3omk = Molecule.findByName("3OMK") ?:
			new Molecule(name: "3OMK", fileNamePdb: "3OMK_1biol_ligand.pdb").save()
			
	}
	
}
