package it.pharm.med.conf

import it.pharm.med.pharmer.PharmerService
import it.pharm.med.FileService
import it.pharm.med.file.MatchFile
import it.pharm.med.model.Molecule

class PharmerTemplate {

	public static void init(pharmerService) {
		
		if (String.metaClass.getMetaMethod("grom")) "setting up pharmer".grom()
	
		def m1upv = Molecule.findByName("1UPV")
		def m1sqn = Molecule.findByName("1SQN")
		def m1zdt = Molecule.findByName("1ZDT")
		def m3gws = Molecule.findByName("3GWS")
		def m3ilz = Molecule.findByName("3ILZ")
		def m3omk = Molecule.findByName("3OMK")
		
		pharmerService.add(m1upv)
		pharmerService.add(m1sqn)
		pharmerService.add(m1zdt)
		pharmerService.add(m3gws)
		pharmerService.add(m3ilz)
		pharmerService.add(m3omk)

	}
	
}
