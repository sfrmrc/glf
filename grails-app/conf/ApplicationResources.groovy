modules = {
    application {
		dependsOn 'bootstrap, bootstrap-responsive-less, jquery, jquery-ui, underscore'
        resource url:'js/application.js'
        resource url:'js/jquery.metadata.js'
        resource url:'js/jquery.chosen.js'
        resource url:'js/jquery.ui.writetable.js'
		resource url: 'css/base.css'
		resource url: 'css/chosen.css'
    }
	jmol {
		resource url:'js/jmol/Jmol.js', disposition: 'head'
	}
	'custom-bootstrap' {
		dependsOn 'bootstrap'
		resource url:[dir: 'less', file: 'custom-bootstrap.less'], attrs:[rel: "stylesheet/less", type:'css']
	}
}