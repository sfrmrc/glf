import grails.util.Environment;
import it.pharm.med.conf.MolecularTemplate;
import it.pharm.med.conf.PharmerTemplate;
import it.pharm.med.model.Food
import it.pharm.med.model.Group
import it.pharm.med.model.Molecule;
import it.pharm.med.model.SubGroup;
import org.codehaus.groovy.grails.commons.GrailsApplication

class BootStrap {

	def grailsApplication
	def pharmerService
	
    def init = { servletContext ->

		if (String.metaClass.getMetaMethod("grom")) "bootstrapping application".grom()
		
		// get configuration
		def config = grailsApplication.config
		
		if(grails.util.GrailsUtil.isDevelopmentEnv()) {
		
			MolecularTemplate.init()
			
			PharmerTemplate.init(pharmerService)

		}
					
    }
	
    def destroy = {
		if (String.metaClass.getMetaMethod("grom")) "stopping application...".grom()
    }
	
}
