<%@ page import="it.pharm.med.model.SubGroup" %>



<div class="fieldcontain ${hasErrors(bean: subGroupInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="subGroup.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${subGroupInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subGroupInstance, field: 'group', 'error')} required">
	<label for="group">
		<g:message code="subGroup.group.label" default="Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="group" name="group.id" from="${it.pharm.med.model.Group.list()}" optionKey="id" required="" value="${subGroupInstance?.group?.id}" class="many-to-one"/>
</div>

