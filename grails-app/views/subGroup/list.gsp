
<%@ page import="it.pharm.med.model.SubGroup" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subGroup.label', default: 'SubGroup')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>

		<g:render template="/navigation/controller/top" />			

		<div class="content">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<table class="table table-striped">
				<thead>
					<tr>

						<th></th>
					
						<g:sortableColumn property="name" title="${message(code: 'subGroup.name.label', default: 'Name')}" />
					
						<th class="header"><g:message code="subGroup.group.label" default="Group" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${subGroupInstanceList}" var="subGroupInstance">
					<tr>

						<td class="link">
							<g:link action="show" id="${subGroupInstance.id}" class="btn btn-small">
								<i class="icon-eye-open"></i>
							</g:link>
						</td>
					
						<td>${fieldValue(bean: subGroupInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: subGroupInstance, field: "group")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${subGroupInstanceTotal}" />
			</div>
		</div>

	</body>
</html>
