
<%@ page import="it.pharm.med.model.SubGroup" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subGroup.label', default: 'SubGroup')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		
		<g:render template="/navigation/controller/top"/>
			
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<dl>
			
				<g:if test="${subGroupInstance?.name}">
					<dt><g:message code="subGroup.name.label" default="Name" /></dt>
					
						<dd><g:fieldValue bean="${subGroupInstance}" field="name"/></dd>
					
				</g:if>
			
				<g:if test="${subGroupInstance?.group}">
					<dt><g:message code="subGroup.group.label" default="Group" /></dt>
					
						<dd><g:link controller="group" action="show" id="${subGroupInstance?.group?.id}">${subGroupInstance?.group?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
			</dl>

			<g:form>
				<g:hiddenField name="id" value="${subGroupInstance?.id}" />
				<div class="form-actions">
					<g:link class="btn" action="edit" id="${subGroupInstance?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<button class="btn btn-danger" type="submit" name="_action_delete">
						<i class="icon-trash icon-white"></i>
						<g:message code="default.button.delete.label" default="Delete" />
					</button>
				</div>
			</g:form>

		</div>

	</body>
</html>
