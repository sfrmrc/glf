<%@ page import="it.pharm.med.model.SubGroup" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subGroup.label', default: 'SubGroup')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		
		<g:render template="/navigation/controller/top"/>
			
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${subGroupInstance}">
			<bootstrap:alert class="alert-error">
			<ul>
				<g:eachError bean="${subGroupInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</bootstrap:alert>
			</g:hasErrors>

			<fieldset>
				<g:form class="form-horizontal" action="edit" id="${subGroupInstance?.id}" >
					<g:hiddenField name="version" value="${subGroupInstance?.version}" />
					<fieldset>
						<f:all bean="subGroupInstance"/>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">
								<i class="icon-ok icon-white"></i>
								<g:message code="default.button.update.label" default="Update" />
							</button>
							<button type="submit" class="btn btn-danger" name="_action_delete" formnovalidate>
								<i class="icon-trash icon-white"></i>
								<g:message code="default.button.delete.label" default="Delete" />
							</button>
						</div>
					</fieldset>
				</g:form>
			</fieldset>

	</div>
	</body>
</html>
