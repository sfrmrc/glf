
<%@ page import="it.pharm.med.model.Group" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
			
		<g:render template="/navigation/controller/top" />
		
		<div class="content">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<table class="table table-striped">
				<thead>
					<tr>

						<th></th>
					
						<g:sortableColumn property="name" title="${message(code: 'group.name.label', default: 'Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${groupInstanceList}" var="groupInstance">
					<tr>

						<td class="link">
							<g:link action="show" id="${groupInstance.id}" class="btn btn-small">
								<i class="icon-eye-open"></i>
							</g:link>
						</td>
					
						<td>${fieldValue(bean: groupInstance, field: "name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${groupInstanceTotal}" />
			</div>
		</div>

	</body>
</html>
