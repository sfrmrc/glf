
<%@ page import="it.pharm.med.model.Group" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		
		<g:render template="/navigation/controller/top"/>
			
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<dl>
			
				<g:if test="${groupInstance?.name}">
					<dt><g:message code="group.name.label" default="Name" /></dt>
					
						<dd><g:fieldValue bean="${groupInstance}" field="name"/></dd>
					
				</g:if>
			
			</dl>

			<g:form>
				<g:hiddenField name="id" value="${groupInstance?.id}" />
				<div class="form-actions">
					<g:link class="btn" action="edit" id="${groupInstance?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<button class="btn btn-danger" type="submit" name="_action_delete">
						<i class="icon-trash icon-white"></i>
						<g:message code="default.button.delete.label" default="Delete" />
					</button>
				</div>
			</g:form>

		</div>
	</body>
</html>
