<!doctype html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>GLF Project - Food/MoleculeStudy</title>
	</head>

	<body>
		<div class="row-fluid">
			<aside id="application-status" class="span3">
				<div class="well sidebar-nav">
					<h5>Application Status</h5>
					<ul>
						<li>App version: <g:meta name="app.version"/></li>
						<li>Grails version: <g:meta name="app.grails.version"/></li>
						<li>JVM version: ${System.getProperty('java.version')}</li>
					</ul>
					<h5>Domain</h5>
					<ul>
						<li>Study</li>
						<li>Food</li>
						<li>Molecule</li>
					</ul>
				</div>
			</aside>

			<section id="main" class="span9">

				<div class="hero-unit">
					<h1>Welcome to ${org.codehaus.groovy.grails.commons.ConfigurationHolder.config.module.name}</h1>
				</div>
					
				<div class="row-fluid">
					<div class="span4">
						<h2>Study</h2>
						<p>Create your Study ....</p>
						<pre>Study = Foods + Molecules</pre>
					</div>
					<div class="span4">
						<h2>Food</h2>
						<p>Create Foods ....</p>
						<pre>Food = Molecules</pre>
					</div>
					<div class="span4">
						<h2>Molecule</h2>
						<p>Create Molecules ....</p>
						<pre>Molecule = :-D</pre>
					</div>
				</div>

			</section>
		</div>
		
	</body>
</html>
