
<%@ page import="it.pharm.med.model.Molecule" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'molecule.label', default: 'Molecule')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		
		<r:script>

			var $templatePC = Handlebars.compile($('#pubchem-molecule').html());

			function showPubchemResult(data) {

				$('#pubchemResult').empty();
				$('#pubchemResult').html($templatePC(data));
				
			}
			
			function beforeSearch() {
				$('#pubchemResult').html('Loading...');
			}
		
		</r:script>
		
	</head>
	<body>

		<g:render template="/navigation/controller/top" />

		<div class="content">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<g:formRemote name="pubchemSearchForm" url="[controller: 'pubchem', action: 'search']"
					before="beforeSearch()"
					onSuccess="showPubchemResult(data)" >
				<div class="input-append">
					<g:textField class="input-large" placeholder="Molecule" name="name" required="true" />
					<button type="submit" class="btn btn-primary" >
						<i class="icon-search icon-white"></i>
					</button>
				</div>
			</g:formRemote>
			
			<div id="pubchemResult" class="span12" >
			</div>

		</div>

		<script id="pubchem-molecule" type="text/x-handlebars-template">

			<ul class="thumbnails">
				<li class="span4">
					<div class="thumbnail">
						<img src="http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{{cid}}/PNG?image_size=160x160" />
						<h3>{{atoms.element}}</h3>
						<p>
							<ul class="unstyled">
							{{#props}}
								<li>{{label}}: {{value}}</li>
							{{/props}}
							</ul>
						</p>
						<p>
							<a class="btn btn-info" href="${createLink(controller: 'pubchem', action: 'download')}/{{cid}}" >Download SDF</a>
						</p>
					</div>
				</li>
			</ul>

		</script>
		
	</body>
</html>
