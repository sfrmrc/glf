<%@ page import="it.pharm.med.model.Molecule" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'molecule.label', default: 'Molecule')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
	
		<g:render template="/navigation/controller/top" />
	
		<div class="content">
			
			<div class="page-header">
				<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${moleculeInstance}">
			<bootstrap:alert class="alert-error">
			<ul>
				<g:eachError bean="${moleculeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</bootstrap:alert>
			</g:hasErrors>

			<fieldset>
				<g:uploadForm class="form-horizontal" action="create" >
					<fieldset>
						<f:all except="foods,molecularWeight,molecularFormula,fileNameSdf" bean="moleculeInstance"/>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">
								<i class="icon-ok icon-white"></i>
								<g:message code="default.button.create.label" default="Create" />
							</button>
						</div>
					</fieldset>
					<g:each in="${extraparams}" var="param">
                  		<input type="hidden" name="${param.key}" value="${param.value}">
                	</g:each>
				</g:uploadForm>
			</fieldset>
			
		</div>

	</body>
</html>
