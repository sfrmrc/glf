<%@ page import="it.pharm.med.model.Molecule" %>



<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="molecule.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${moleculeInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'alias', 'error')} ">
	<label for="alias">
		<g:message code="molecule.alias.label" default="Alias" />
		
	</label>
	<g:textField name="alias" value="${moleculeInstance?.alias}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'fileNamePdb', 'error')} ">
	<label for="fileNamePdb">
		<g:message code="molecule.fileNamePdb.label" default="File Name Pdb" />
		
	</label>
	<g:textField name="fileNamePdb" value="${moleculeInstance?.fileNamePdb}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'molecularFormula', 'error')} ">
	<label for="molecularFormula">
		<g:message code="molecule.molecularFormula.label" default="Molecular Formula" />
		
	</label>
	<g:textField name="molecularFormula" value="${moleculeInstance?.molecularFormula}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'molecularWeight', 'error')} ">
	<label for="molecularWeight">
		<g:message code="molecule.molecularWeight.label" default="Molecular Weight" />
		
	</label>
	<g:textField name="molecularWeight" value="${moleculeInstance?.molecularWeight}"/>
</div>

