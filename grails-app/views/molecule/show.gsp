
<%@ page import="it.pharm.med.model.Molecule" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'molecule.label', default: 'Molecule')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

		<g:render template="/navigation/controller/top" />

		<div class="content">
			
				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<g:if test="${flash.pharmerMessage}">
				<bootstrap:alert class="alert-info">${flash.pharmerMessage}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${moleculeInstance?.name}">
						<dt><g:message code="molecule.name.label" default="Name" /></dt>
						
							<dd><g:fieldValue bean="${moleculeInstance}" field="name"/></dd>
						
					</g:if>
				
					<g:if test="${moleculeInstance?.alias}">
						<dt><g:message code="molecule.alias.label" default="Alias" /></dt>
						
							<dd><g:fieldValue bean="${moleculeInstance}" field="alias"/></dd>
						
					</g:if>
				
					<g:if test="${moleculeInstance?.fileNamePdb}">
						<dt><g:message code="molecule.fileNamePdb.label" default="File Name Pdb" /></dt>
						
							<dd><g:fieldValue bean="${moleculeInstance}" field="fileNamePdb"/></dd>
							<dd><g:render template="jmol" model="['moleculeInstance': moleculeInstance]" /></dd>
						
					</g:if>
				
					<g:if test="${moleculeInstance?.molecularFormula}">
						<dt><g:message code="molecule.molecularFormula.label" default="Molecular Formula" /></dt>
						
							<dd><g:fieldValue bean="${moleculeInstance}" field="molecularFormula"/></dd>
						
					</g:if>
				
					<g:if test="${moleculeInstance?.molecularWeight}">
						<dt><g:message code="molecule.molecularWeight.label" default="Molecular Weight" /></dt>
						
							<dd><g:fieldValue bean="${moleculeInstance}" field="molecularWeight"/></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${moleculeInstance?.id}" />
					<div class="form-actions">
						<g:link params="${extraparams}" class="btn" action="edit" id="${moleculeInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
					<g:each in="${extraparams}" var="param">
						<input type="hidden" name="${param.key}" value="${param.value}">
					</g:each>
				</g:form>

			</div>

	</body>
</html>
