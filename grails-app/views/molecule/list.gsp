
<%@ page import="it.pharm.med.model.Molecule" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'molecule.label', default: 'Molecule')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>

		<g:render template="/navigation/controller/top" />

		<div class="content">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<table class="table table-striped">
				<thead>
					<tr>

						<th></th>
					
						<g:sortableColumn params="${extraparams}" property="name" title="${message(code: 'molecule.name.label', default: 'Name')}" />
					
						<g:sortableColumn params="${extraparams}" property="alias" title="${message(code: 'molecule.alias.label', default: 'Alias')}" />
					
						<g:sortableColumn params="${extraparams}" property="fileNamePdb" title="${message(code: 'molecule.fileNamePdb.label', default: 'File Name Pdb')}" />
					
						<g:sortableColumn params="${extraparams}" property="molecularFormula" title="${message(code: 'molecule.molecularFormula.label', default: 'Molecular Formula')}" />
					
						<g:sortableColumn params="${extraparams}" property="molecularWeight" title="${message(code: 'molecule.molecularWeight.label', default: 'Molecular Weight')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${moleculeInstanceList}" var="moleculeInstance">
					<tr>

						<td class="link">
							<g:link params="${extraparams}" action="show" id="${moleculeInstance.id}" class="btn btn-small">
								<i class="icon-eye-open"></i>
							</g:link>
						</td>
					
						<td>${fieldValue(bean: moleculeInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: moleculeInstance, field: "alias")}</td>
					
						<td>${fieldValue(bean: moleculeInstance, field: "fileNamePdb")}</td>
					
						<td>${fieldValue(bean: moleculeInstance, field: "molecularFormula")}</td>
					
						<td>${fieldValue(bean: moleculeInstance, field: "molecularWeight")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${moleculeInstanceTotal}" />
			</div>
		</div>

	</body>
</html>
