
<g:if test="${moleculeInstance?.fileNamePdb}" >

	<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'fileNamePdb', 'error')} required">
		<label for="name">
			<g:message code="molecule.fileNamePdb.label" default="File Name Pdb" />
		</label>
		<f:display property="fileNamePdb" bean="moleculeInstance" />
	</div>

</g:if>

<div class="fieldcontain ${hasErrors(bean: moleculeInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="molecule.filePdb.label" default="File PDB" />
	</label>
	<input type="file" name="pdbFile" />
</div>
