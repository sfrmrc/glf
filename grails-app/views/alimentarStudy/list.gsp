
<%@ page import="it.pharm.med.model.AlimentarStudy" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'alimentarStudy.label', default: 'AlimentarStudy')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>

		<div class="container">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<table class="table table-striped">
				<thead>
					<tr>

						<th></th>
					
						<g:sortableColumn property="name" title="${message(code: 'study.name.label', default: 'Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${alimentarStudyInstanceList}" var="alimentarStudyInstance">
					<tr>

						<td class="link">
							<g:link action="show" id="${alimentarStudyInstance.id}" class="btn btn-small">
								<i class="icon-eye-open"></i>
							</g:link>
						</td>
					
						<td>${fieldValue(bean: alimentarStudyInstance, field: "name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${alimentarStudyInstanceTotal}" />
			</div>
		</div>

	</body>
</html>
