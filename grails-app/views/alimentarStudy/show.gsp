
<%@ page import="it.pharm.med.model.AlimentarStudy" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'alimentarStudy.label', default: 'AlimentarStudy')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
			
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<dl>
			
				<g:if test="${alimentarStudyInstance.name}">
					<dt><g:message code="study.name.label" default="Name" /></dt>
					
						<dd><g:fieldValue bean="${alimentarStudyInstance}" field="name"/></dd>
					
				</g:if>
				
				<dt><g:message code="alimentarStudy.foods.label" default="Foods" /></dt>
				<g:if test="${alimentarStudyInstance?.foods}">
					
					<g:each in="${alimentarStudyInstance.foods}" var="s">
					<dd><g:link controller="food" action="show" id="${s.food.id}">${s?.encodeAsHTML()}</g:link></dd>
					</g:each>
					
				</g:if>

				<dt><g:message code="alimentarStudy.molecules.label" default="Molecules" /></dt>				
				<g:if test="${alimentarStudyInstance?.molecules}">

					<g:each in="${alimentarStudyInstance.molecules}" var="m">
					<dd><g:link controller="molecule" action="show" id="${m.molecule.id}">${m?.encodeAsHTML()}</g:link></dd>
					</g:each>
					
				</g:if>
			
			</dl>

			<div class="form-actions">
				<a class="btn btn-small" href="${gscfLink}" >More &raquo;</a>
			</div>				

		</div>

	</body>
</html>
