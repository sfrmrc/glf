<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><g:layoutTitle default="${meta(name: 'appName')}"/></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="initial-scale = 1.0">

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<r:require modules="bootstrap, bootstrap-responsive-less"/>
		<r:require modules="application, jmol"/>
		
		<r:script disposition="head" >
			jmolInitialize("<g:resource dir="applets"/>", false);
		</r:script>
		
		<r:layoutResources/>
	</head>
	<body>
		<g:layoutBody/>
		<r:layoutResources/>
	</body>
</html>