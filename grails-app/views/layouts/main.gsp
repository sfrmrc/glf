<%@ page import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes" %>
<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><g:layoutTitle default="${meta(name: 'appName')}"/></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="initial-scale = 1.0">

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<r:require modules="bootstrap, bootstrap-responsive-less"/>
		<r:require modules="application, jmol"/>
		<script type="text/javascript" src="${resource(dir:'js', file:'handlebars-1.0.rc.1.min.js')}" ></script>
		
		<r:script disposition="head" >
			jmolInitialize("<g:resource dir="applets"/>", false);
		</r:script>
		
		<r:layoutResources/>

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">

		<g:layoutHead/>
		
	</head>

	<body>

		<nav class="navbar">
			<g:render template="/navigation/topnav"/>
		</nav>
		
		<div class="container">
		
			<g:layoutBody/>

		</div>
		
		<footer class="footer">
			
			<div class="container" >
				<p>Designed and built with <a href="http://www.grails.org" target="_blank" >Grails</a></p>
				<p>Published with <a href="https://bitbucket.org/" target="_blank" >Bitbucket</a></p>
			</div>
			
		</footer>

		<r:layoutResources/>

	</body>
</html>