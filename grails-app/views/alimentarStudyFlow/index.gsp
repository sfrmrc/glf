<html>
<head>
	<meta name="layout" content="main">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'alimentarStudyFlow.css')}"/>
	<title><g:message code="study.flow.title" default="Study Flow Index" /></title>
</head>
<body>

	<div class="container">
	
		<g:render template="common/ajaxflow"/>
				
	</div>
</body>
</html>
