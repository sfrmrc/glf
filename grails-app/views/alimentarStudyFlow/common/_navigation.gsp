<%
/**
 * Navigation template
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<g:set var="showPrevious" value="${page>1 && page<pages.size}"/>
<g:set var="showNext" value="${page>1 && page<pages.size}"/>
<af:navigation events="[previous:[label:'&laquo; prev',show: showPrevious, className: 'btn'], next:[label:'next &raquo;', show:showNext]]" separator="&nbsp; | &nbsp;" class="btn btn-primary" />
