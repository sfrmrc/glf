<dt><g:message code="alimentarStudy.foods.label" default="Foods" /></dt>
	
<dd>
	<button class="btn btn-small btn-success" type="button" id="addFoodLink" name="_action_add">
		<i class="icon-plus icon-white"></i>
		<g:message code="default.button.add.label" default="Add" />
	</button>
	<g:link controller="food" action="create" params="[dialog: 'true']" class="btn btn-small btn" elementId="createFoodLink" name="_action_create" >
		<g:message code="default.button.create.label" default="Create" />
	</g:link>

	<table class="table table-striped" data="{tableName:'foods'}" id="foods" >
	
		<thead>
			<tr>
				<th data="{required:true, name:'food.name', id:'food', placeholder:'Required'}" >Name</th>
				<th data="{editable: false}">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			
			<g:each in="${alimentarStudyInstance.foods}" var="f" status="i" >
				<tr rowId="${i}" >
					<td>${f.food.name}</td>
					<td>
						<button class="btn btn-small delete-row-button" type="button" >
							<i class="icon-minus"></i>
						</button>
					</td>
				</tr>
			</g:each>
			
		</tbody>
	
	</table>

</dd>

<script type="text/javascript" >

	$(function() {
	
		function attachTableRowFoodAutocomplete(parent) {
		
			console.debug(parent);
			
			$(parent).find('input[name*="food.name"]').each(function(i, e) {
				$(e).autocomplete({ 
					source: "${createLink(action:'ajaxSearch', controller: 'food')}"
				})
			})
		
		}

		modal($("#createFoodLink"));
	
		attachTableMetadata('foods', 'addFoodLink', attachTableRowFoodAutocomplete);
		attachTableRowFoodAutocomplete('foods');

    });

</script>