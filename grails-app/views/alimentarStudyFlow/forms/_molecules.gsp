<dt><g:message code="alimentarStudy.foods.label" default="Foods" /></dt>
	
<dd>
	<table class="table table-striped" >
		<thead>
			<tr>
				<th><g:message code="food.foodMolecule.foods.name.label" default="Name" /></th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${alimentarStudyInstance.foods}" var="f">
				<tr>
					<td>${f?.encodeAsHTML()}</td>
				</tr>
			</g:each>										
		</tbody>
	</table>
</dd>

<dt><g:message code="alimentarStudy.molecules.label" default="Molecules" /></dt>

<dd>
	<button class="btn btn-small btn-success" type="button" id="addMoleculeLink" name="_action_add">
		<i class="icon-plus icon-white"></i>
		<g:message code="default.button.add.label" default="Add" />
	</button>
	<g:link controller="molecule" action="create" params="[dialog: 'true']" class="btn btn-small btn" elementId="createMoleculeLink" name="_action_create" >
		<g:message code="default.button.create.label" default="Create" />
	</g:link>

	<table class="table table-striped" data="{tableName:'molecules'}" id="molecules" >
	
		<thead>
			<tr>
				<th data="{required:true, name:'molecule.name', id:'molecule', placeholder:'Required'}" >Name</th>
				<th data="{editable: false}">&nbsp;</th>
			</tr>
		</thead>
		
		<tbody>
			
			<g:each in="${alimentarStudyInstance.molecules}" var="m" status="i" >
				<tr rowId="${i}" >
					<td>${m.molecule.name}</td>
					<td>
						<button class="btn btn-small delete-row-button" type="button" >
							<i class="icon-minus"></i>
						</button>
					</td>
				</tr>
			</g:each>
			
		</tbody>
	
	</table>

</dd>

<script type="text/javascript" >

	$(function() {
	
		function attachTableRowMoleculeAutocomplete(parent) {
		
			console.debug(parent);
			
			$(parent).find('input[name*="molecule.name"]').each(function(i, e) {
				$(e).autocomplete({ 
					source: "${createLink(action:'ajaxSearch', controller: 'molecule')}"
				})
			})
		
		}

		modal($('#createMoleculeLink'));
	
		attachTableMetadata('molecules', 'addMoleculeLink', attachTableRowMoleculeAutocomplete);
		attachTableRowMoleculeAutocomplete('molecules');

    });

</script>