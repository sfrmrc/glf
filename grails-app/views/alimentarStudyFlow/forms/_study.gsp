<%@ page import="org.dbxp.moduleBase.Study" %>
<g:set var="entityName" value="${message(code: 'study.label', default: 'Study')}" />
<div class="page-header">
	
	<h1><g:message code="default.list.label" args="[entityName]" /></h1>

	<g:if test="${flash.message}">
	<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
	</g:if>
	
	<table class="table table-striped">
		<thead>
			<tr>
			
				<th><g:message code="study.name.label" default="Name" /></th>
				<th><g:message code="study.foods.label" default="Food" /></th>
				<th><g:message code="study.molecules.label" default="Molecule" /></th>
				
				<th></th>
				
			</tr>
		</thead>
		<tbody>
		<g:each in="${alimentarStudyInstanceList}" var="alimentarStudyInstance">
			<tr>
			
				<td>${fieldValue(bean: alimentarStudyInstance, field: "name")}</td>
				<td>${fieldValue(bean: alimentarStudyInstance, field: "foods")}</td>
				<td>${fieldValue(bean: alimentarStudyInstance, field: "molecules")}</td>
			
				<td class="link">
					<af:ajaxButton name="next" value="Edit" id="${fieldValue(bean: alimentarStudyInstance, field: "id")}" afterSuccess="onPage();" class="btn btn-small" />
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bootstrap:paginate total="${alimentarStudyInstanceTotal}" />
	</div>
		
</div>
