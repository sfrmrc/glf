<af:page>

	<dl>

		<dd>
			<g:if test="${alimentarStudyInstance?.name}">
				<dt><g:message code="study.description.label" default="Name" /></dt>
				
					<dd><g:fieldValue bean="${alimentarStudyInstance}" field="name"/></dd>
				
			</g:if>
		</dd>	
		
		<dd>
			<g:render template="forms/foods" />
		</dd>
		
	</dl>
	

</af:page>