<af:page>

	<g:if test="${alimentarStudyInstance?.name}">
		<dt><g:message code="study.description.label" default="Name" /></dt>
		
			<dd><g:fieldValue bean="${alimentarStudyInstance}" field="name"/></dd>
		
	</g:if>

	<g:render template="forms/molecules" />

</af:page>