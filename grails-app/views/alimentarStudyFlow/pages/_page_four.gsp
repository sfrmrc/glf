<af:page>

	<dl>
				
		<dt><g:message code="study.description.label" default="Name" /></dt>
		<dd><g:fieldValue bean="${alimentarStudyInstance}" field="name"/></dd>
		
	
		<dt><g:message code="study.foods.label" default="Food" /></dt>
		<g:each in="${alimentarStudyInstance.foods}" var="f">
		<dd><g:link controller="food" action="show" id="${f.food.id}">${f?.encodeAsHTML()}</g:link></dd>
		</g:each>
		
		<dt><g:message code="study.molecules.label" default="Molecule" /></dt>
		<g:each in="${alimentarStudyInstance.molecules}" var="m">
		<dd><g:link controller="molecule" action="show" id="${m.molecule.id}">${m?.encodeAsHTML()}</g:link></dd>
		</g:each>
	
	</dl>

</af:page>