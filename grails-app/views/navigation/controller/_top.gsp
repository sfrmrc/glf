<nav class="nav" >
	<ul class="nav nav-pills">
		<li <%= request.forwardURI.indexOf('/list')!=-1 ? 'class="active"' : '' %> >
			<g:link class="list" action="list" params="${extraparams}" >
				<i class="icon-list" ></i> <g:message code="default.list.label" args="[entityName]" />
			</g:link>
		</li>
		<li <%= request.forwardURI.indexOf('/create')!=-1 || request.forwardURI.indexOf('/show')!=-1 ? 'class="active"' : '' %> >
			<g:link class="create" action="create" params="${extraparams}">
				<i class="icon-plus" ></i> <g:message code="default.create.label" args="[entityName]" />
			</g:link>
		</li>
	</ul>
</nav>