<script type="text/javascript" >

$(document).ready(function() {

	$("#search_study").autocomplete({

		minLength: 2,
		delay: 300,
		html: true,
		source: function(request, response) {

			$.ajax({
				url: "${createLink(controller: 'home', action:'ajaxStudySearch')}",
				dataType: "jsonp",
				data: {
					featureClass: "P",
					style: "full",
					maxRows: 12,
					query: request.term
				},
				success: function(data) {
					response($.map(data.data, function(item) {
						return {
							label		: item.category+' '+item.name,
							value		: item.link
						}
					}));
				}
			});
		},

		select: function(event, ui) {
			if (ui.item.value) {
				this.value = ui.item.label;
				window.location = ui.item.value;
			}
		}
		
	});
	
});

</script>

<div class="navbar-inner">
	<div class="container-fluid">

		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="nav-collapse">

			<ul class="nav">
			
				<li <%= request.forwardURI.equals(createLink(uri: '/')) ? 'class="active"' : '' %> >
					<a href="${createLink(uri: '/')}">
						<i class="icon-home icon-white" ></i>
					</a>
				</li>

				<li>
					<g:link url="${org.codehaus.groovy.grails.commons.ConfigurationHolder.config.gscf.baseURL}">GSCF</g:link>
				</li>
				
				<li class="dropdown">
				
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						&nbsp;<g:message code="nav.menu.pharmer" default="Pharmer" /><b class="caret"></b>
					</a>
					
					<ul class="dropdown-menu">
						<li>
							<g:link controller="pharmer" action="index">
								<i class="icon-search" ></i>
								&nbsp;<g:message code="nav.menu.pharmer.search" default="Search" />
							</g:link>
						</li>
						<li>
							<g:link controller="pharmer" action="config">
								<i class="icon-cog" ></i>
								&nbsp;<g:message code="nav.menu.pharmer.config" default="Configuration" />
							</g:link>
						</li>
					</ul>
					
				</li>
				
				<li <%= request.forwardURI.indexOf('/molecule/')!=-1 ? 'class="active"' : '' %> >
					<g:link controller="molecule"><g:message code="nav.menu.molecule" default="Molecule" /></g:link>
				</li>
				<li <%= request.forwardURI.indexOf('/food/')!=-1 ? 'class="active"' : '' %> >
					<g:link controller="food"><g:message code="nav.menu.food" default="Food" /></g:link>
				</li>
				<li <%= request.forwardURI.indexOf('/alimentarStudy/')!=-1 ? 'class="active"' : '' %> >
					<g:link controller="alimentarStudy"><g:message code="nav.menu.alimStudy" default="Alimentar Study" /></g:link>
				</li>
				<li <%= request.forwardURI.indexOf('/alimentarStudyFlow/')!=-1 ? 'class="active"' : '' %> >
					<g:link controller="alimentarStudyFlow"><g:message code="nav.menu.alimStudyFlow" default="Alimentar Study Flow" /></g:link>
				</li>

				<li>
					<div class="navbar-search pull-right" >
						<input type="text" class="search-query span2" id="search_study" placeholder="Search Study">
					</div>
				</li>
			
				<li class="divider-vertical"></li>
				
				<g:if test="${!session?.user}">
							
					<li>
						<g:link controller="login" action="index" >
							&nbsp;<g:message code="default.button.login" default="Login" />
						</g:link>
					</li>
					
				</g:if>
				<g:else>
			
					<li class="dropdown">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							&nbsp;<img src="http://placehold.it/20x20"><b class="caret"></b>
						</a>
						
						<ul class="dropdown-menu">
							<li><a href="#"><i class="icon-user" ></i>&nbsp;${session?.user?.username}</a></li>
							<li>
								<g:link controller="alimentarStudy" action="synchronize">
	                       			<i class="icon-refresh" ></i>
	                       			&nbsp;<g:message code="default.button.synchronize" default="Synchronize" />
			                    </g:link>
							</li>
							<li class="divider"></li>
							<li>
								<g:link controller="logout" action="index">
									<i class="icon-off" ></i>
									&nbsp;<g:message code="default.button.logout" default="Logout" />
								</g:link>
							</li>
						</ul>
						
					</li>
					
				</g:else>					
			
			</ul>

		</div>
	</div>
</div>
