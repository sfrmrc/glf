<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pharmer.label', default: 'PharmerSearch')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="search" action="index">
								<i class="icon-list"></i>
								<g:message code="pharmer.search.list" default="Search" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="pharmer.config.title" default="Pharmer Config Panel" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-error">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<dt><g:message code="pharmer.config.opts1" default="Option 1" /></dt>
					
					<g:each in="${pharmerDbInstanceList}" var="p" >
						<dd>${p.name} - ${p.fileNamePdb} - ${p.output}</dd>
					</g:each>
				
				</dl>

			</div>

		</div>
	</body>
</html>
