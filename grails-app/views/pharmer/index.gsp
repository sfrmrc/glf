<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'pharmer.label', default: 'PharmerSearch')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		
		<script type="text/javascript" >
		
			$(function() {
			
				$(document).find('input[name*="molecule.name"]').each(function(i, e) {
					$(e).autocomplete({ 
						source: "${createLink(action:'ajaxSearch', controller: 'molecule')}"
					})
				})
			
			});

			function showPharmerResult(response) {

				$('#pharmerResult').empty();
				$('#jmolResult').empty();

				try{
				
					if(response===null || response.length==0) {
						$('#pharmerResult').empty();
						var row = $('<div class="alert alert-block alert-info">${message(code:'pharmer.search.nodatafound',default:'No data found')}</div>');
						$('#pharmerResult').append(row);
						return;
					}
	
					var matchInfo = response.matchInfo;
					
					for(var i=0; matchInfo!==null && i<matchInfo.length; i++) {
	
						var link = '${createLink(controller: 'molecule', action:'show')}/'+matchInfo[i].link;
						var row = $("<dt>Molecule: <a href='"+link+"'>"+matchInfo[i].moleculeName+"</a></dt><dd>PDB: "+matchInfo[i].filenamePdb+"</dd><dd>RMSD: "+matchInfo[i].rmsd+"</dd>");
						$('#pharmerResult').append(row);
					
					}
	
					if(matchInfo!==null && matchInfo.length!=0) {
						jmolSetXHTML('jmolResult');
						jmolApplet(400, "load <g:resource dir="match"/>/"+response.fileResult+";");
					}else{
						showNothingFound();
					}

				}catch(e){
					console.log(response);
					console.log(e);
					showPharmerError(response);
				}

			}
			
			function showPharmerError(response) {
				$('#pharmerResult').empty();
				var row = $('<div class="alert alert-block alert-error">${message(code:'pharmer.search.error',default:'Unable to start service')}</div>');
				$('#pharmerResult').append(row);

				if(response.message){
					var message = $('<div class="alert alert-block alert-error">'+response.message+'</div>');
					$('#pharmerResult').append(message);
				}
				
			}

			function showNothingFound() {
				$('#pharmerResult').empty();
				var row = $('<div class="alert alert-block alert-info">${message(code:'default.not.found.data.message',default:'Nothing to display',args:['Molecule'])}</div>');
				$('#pharmerResult').append(row);
			}

		</script>
		
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="search" action="index">
								<i class="icon-list"></i>
								<g:message code="pharmer.search.list" default="Search" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="pharmer.search.title" default="Pharmer Search" /></h1>
				</div>

				<div class="row-fluid">

					<div class="span12">

						<g:formRemote name="pharmerSearchForm" class="form-inline" onFailure="showPharmerError()"
							url="[controller:'pharmer', action:'search']" onSuccess="showPharmerResult(data)" >
									<g:message code="molecule.name.label" default="Name" /></dt>
									<g:textField placeholder="Molecule" name="molecule.name" required="true" value="${molecule?.name}"/>
									<button type="submit" class="btn btn-primary">
										<i class="icon-search icon-white"></i>
									</button>
						</g:formRemote>

					</div>
	
					<div class="row-fluid">
						<div class="span6" >
							<dl id="pharmerResult" ></dl>
						</div>
						<div id="jmolResult" class="span6" ></div>
					</div>

				</div>
				
			</div>

		</div>
		
	</body>
</html>
