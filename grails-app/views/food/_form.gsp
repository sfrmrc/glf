<%@ page import="it.pharm.med.model.Food" %>



<div class="fieldcontain ${hasErrors(bean: foodInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="food.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${foodInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodInstance, field: 'molecules', 'error')} ">
	<label for="molecules">
		<g:message code="food.molecules.label" default="Molecules" />
		
	</label>
	<g:select name="molecules" from="${it.pharm.med.model.Molecule.list()}" multiple="multiple" optionKey="id" size="5" value="${foodInstance?.molecules*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodInstance, field: 'subGroup', 'error')} required">
	<label for="subGroup">
		<g:message code="food.subGroup.label" default="Sub Group" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="subGroup" name="subGroup.id" from="${it.pharm.med.model.SubGroup.list()}" optionKey="id" required="" value="${foodInstance?.subGroup?.id}" class="many-to-one"/>
</div>

