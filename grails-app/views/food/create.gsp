<%@ page import="it.pharm.med.model.Food" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
			
		<g:render template="/navigation/controller/top"/>
			
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<g:hasErrors bean="${foodInstance}">
			<bootstrap:alert class="alert-error">
			<ul>
				<g:eachError bean="${foodInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</bootstrap:alert>
			</g:hasErrors>

			<fieldset>
				<g:form class="form-horizontal" action="create" >
					<fieldset>
						<f:field property="name" bean="foodInstance"/>
						<f:field property="subGroup" bean="foodInstance"/>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">
								<i class="icon-ok icon-white"></i>
								<g:message code="default.button.create.label" default="Create" />
							</button>
						</div>
					</fieldset>
					<g:each in="${extraparams}" var="param">
                  		<input type="hidden" name="${param.key}" value="${param.value}">
                	</g:each>
				</g:form>
			</fieldset>
		</div>

	</body>
</html>
