
<%@ page import="it.pharm.med.model.Food" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>

		<g:render template="/navigation/controller/top"/>

		<div class="container">
			
			<div class="page-header">
				<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>
			
			<table class="table table-striped">
				<thead>
					<tr>

						<th></th>
					
						<g:sortableColumn params="${extraparams}" property="name" title="${message(code: 'food.name.label', default: 'Name')}" />
					
						<th class="header"><g:message code="food.subGroup.label" default="Sub Group" /></th>

						<th class="header"><g:message code="food.molecules.label" default="Molecules" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${foodInstanceList}" var="foodInstance">
					<tr>

						<td class="link">
							<g:link params="${extraparams}" action="show" id="${foodInstance.id}" class="btn btn-small">
								<i class="icon-eye-open"></i>
							</g:link>
						</td>
					
						<td>${fieldValue(bean: foodInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: foodInstance, field: "subGroup")}</td> 

						<td>${fieldValue(bean: foodInstance, field: "molecules")}</td> 
						
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${foodInstanceTotal}" />
			</div>
		</div>

	</body>
</html>
