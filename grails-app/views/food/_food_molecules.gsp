<ul class="one-to-many" >

	<li>

		<div class="btn-group">
			<button class="btn btn-small btn-success" type="button" id="addMoleculeLink" name="_action_add">
				<i class="icon-plus icon-white"></i>
				<g:message code="default.button.add.label" default="Add" />
			</button>
			<g:link controller="molecule" action="create" params="[dialog: 'true']" class="btn btn-small btn" elementId="createMoleculeLink" name="_action_create" >
				<g:message code="default.button.create.label" default="Create" />
			</g:link>
		</div>
	
	</li>

	<li>
		<table class="table table-striped" data="{tableName:'molecules'}" id="molecules" >
		
			<thead>
				<tr>
					<th data="{required:true, name:'molecule.name', id:'molecule', placeholder:'Required'}" ><g:message code="food.foodMolecule.molecule.name" default="Molecule"/></th>
					<th data="{required:false, name:'amount', id:'amount', placeholder:'Optional'}" ><g:message code="food.foodMolecule.amount" default="Amount"/></th>
					<th data="{editable: false}">&nbsp;</th>
				</tr>
			</thead>
			
			<tbody>
				
				<g:each in="${foodInstance?.molecules}" var="fm" status="i" >
					<tr rowId="${i}" >
						<td>${fm.molecule.name}</td>
						<td><g:formatNumber number="${fm.amount}" formatName="default.number2d.format" /></td>
						<td>
							<button class="btn btn-small delete-row-button" type="button" >
								<i class="icon-minus"></i>
							</button>
						</td>
					</tr>
				</g:each>
				
			</tbody>
		
		</table>
	</li>

</ul>

<r:script>

	$(function() {
	
		function attachTableRowMoleculeAutocomplete(parent) {
		
			console.debug(parent);
			
			$(parent).find('input[name*="molecule.name"]').each(function(i, e) {
				$(e).autocomplete({ 
					source: "${createLink(action:'ajaxSearch', controller: 'molecule')}"
				})
			})
		
		}

		modal($('#createMoleculeLink'));
		
		attachTableMetadata('molecules', 'addMoleculeLink', attachTableRowMoleculeAutocomplete);
		attachTableRowMoleculeAutocomplete('molecules');

    });

</r:script>