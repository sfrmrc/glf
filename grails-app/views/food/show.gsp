
<%@ page import="it.pharm.med.model.Food" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="${layout}" />
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	
		<g:render template="/navigation/controller/top"/>
	
		<div class="container">

			<div class="page-header">
				<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			</div>

			<g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
			</g:if>

			<dl>
			
				<g:if test="${foodInstance?.name}">
					<dt><g:message code="food.name.label" default="Name" /></dt>
					
						<dd><g:fieldValue bean="${foodInstance}" field="name"/></dd>
					
				</g:if>
			
				<g:if test="${foodInstance?.molecules}">
					<dt><g:message code="food.foodMolecule.label" default="Molecules" /></dt>
					
						<dd>
							<table class="table table-striped" >
								<thead>
									<tr>
										<th><g:message code="food.foodMolecule.molecule.name.label" default="Name" /></th>
										<th><g:message code="food.foodMolecule.molecule.amount.label" default="Amount" /></th>
									</tr>
								</thead>
								<tbody>
									<g:each in="${foodInstance.molecules}" var="fm">
										<tr>
											<td>${fm.molecule}</td>
											<td><g:formatNumber number="${fm.amount}" format="0.00" nan="#" /></td>
										</tr>
									</g:each>										
								</tbody>
							</table>
						</dd>
					
				</g:if>
			
				<g:if test="${foodInstance?.subGroup}">
					<dt><g:message code="food.subGroup.label" default="Sub Group" /></dt>
					
						<dd><g:link controller="subGroup" action="show" id="${foodInstance?.subGroup?.id}">${foodInstance?.subGroup?.encodeAsHTML()}</g:link></dd>
					
				</g:if>
			
			</dl>

			<g:form>
				<g:hiddenField name="id" value="${foodInstance?.id}" />
				<div class="form-actions">
					<g:link params="${extraparams}" class="btn" action="edit" id="${foodInstance?.id}">
						<i class="icon-pencil"></i>
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<button class="btn btn-danger" type="submit" name="_action_delete">
						<i class="icon-trash icon-white"></i>
						<g:message code="default.button.delete.label" default="Delete" />
					</button>
				</div>
				<g:each in="${extraparams}" var="param">
					<input type="hidden" name="${param.key}" value="${param.value}">
				</g:each>
			</g:form>

		</div>

	</body>
</html>
