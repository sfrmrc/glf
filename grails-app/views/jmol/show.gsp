<%@ page import="it.pharm.med.model.Molecule" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<title></title>
	</head>
	<body>

		<script type="text/javascript" >
			jmolApplet(400, "load <g:resource dir="pdb"/>/${moleculeInstance?.fileNamePdb};");
		</script>

	</body>
</html>
