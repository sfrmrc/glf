package it.pharm.med.pubchem

class PCCompound implements Serializable {

	transient json
	def important = ['SMILES', 'Molecular Weight', 'Molecular Formula']
	
	def cid
	def atoms
	def elements
	def props = []

	def PCCompound(json) {
		this.json = json.PC_Compounds
		
		cid = this.json['id']['id']['cid']
		elements = this.json['elements'] ? this.json['elements'][0] : []
		atoms = this.json['atoms'] ? this.json['atoms'][0] : []
		
		this.json['props'][0].each { p ->

			println p.value
			if(important.contains(p.urn.label))	
				props << [
					label: p.urn.label,
					value: p.value.sval ? p.value.sval : p.value.fval ? p.value.fval : p.value.ival
				]
			
		}
		
	}

}

class Prop implements Serializable {
	
	def urn
	def value
	
}