package it.pharm.med.pubchem;

import grails.converters.JSON

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.web.json.JSONArray
import org.dbxp.moduleBase.BadRequestException
import org.dbxp.moduleBase.NotAuthenticatedException
import org.dbxp.moduleBase.NotAuthorizedException
import org.dbxp.moduleBase.ResourceNotFoundException

class PubchemService implements Serializable {

	static transactional = false
	
	static jsonResult = "application/json"

	def searchMolecule(molecule) {
		
		def command = PubchemCommand.compoundByName(molecule.name)
		
		def json = call(command, jsonResult)
		
		if(json) {
			def pc = new PCCompound(json[0])
			return pc
		} 
		
		return null
	}
	
	def download(cid) {
		return "${PubchemCommand.getUrl()}/${PubchemCommand.compoundByCid(cid)}/SDF"
	}
	
	private List call(String command, String type) {
		
		def url = "${PubchemCommand.getUrl()}/$command"
		URLConnection connection
		
		try{
			
			log.info "PubChem call - $url"
			log.info "PubChem call with parameters"
			
			connection = url.toURL().openConnection()
			connection.requests.putAt("Accept", type)
			
		}catch(Exception e){
			log.error("PubChem failed when calling service: $url", e)
			throw new Exception("Calling PubChem Rest method $command failed. Please check log for more information.", e)
		}
		
		def result = [:]
		
		switch (connection.responseCode) {
			case 400:    // Bad request
				throw new BadRequestException("Bad request made to server: $url")
				break
			case 401:    // Not allowed to access this resource
				throw new NotAuthorizedException("User is not authorized to access the resource: $url")
				break
			case 403:    // Incorrect authentication
				throw new NotAuthenticatedException("User is not authenticated. Requested URL: $url")
				break
			case 404:    // Resource not found
				throw new ResourceNotFoundException("Specified resource could not be found: $url")
				break
			case 500:    // Internal server error
				throw new Exception("An unknown error occured when calling service: $url response: $connection.responseMessage")
				break
			default:
				try {

					String responseText = connection.content.text

					def jsonResponse = JSON.parse(responseText)

					if (responseText.size() > 2000)
						log.info("PubChem response: ${responseText[0..2000]}...")
					else
						log.info("PubChem response: $responseText")

					if (jsonResponse instanceof JSONArray) {
						
						jsonResponse.collect { jsonElement ->
					
							def m = [:]
							jsonElement.each { m << it }
							[m]
							
						}
					
					}else {
					
						jsonResponse.each { result << it }
						[result]
						
					}

				} catch (Exception e) {
					log.error("Parsing JSON response failed at $url. Reponse was $connection.content.text", e)
					throw new Exception("Parsing JSON response failed at $url.  Please check log for more information.", e)
				}
				break
		}
		
	}
	
}

class PubchemCommand {
	
	static String getUrl() {
		ConfigurationHolder.config.pubchem.url
	}
	
	static String compoundByName(name) {
		"compound/name/$name"
	}
	
	static String compoundByCid(cid) {
		"compound/cid/$cid"
	}
	
}
