package it.pharm.med.pharmer

import it.pharm.med.command.ProcessLauncher
import it.pharm.med.command.exception.CommandNotExistsException
import it.pharm.med.model.PharmerDB

import java.text.MessageFormat

import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class PharmerService {

	def dbFileService
	def pdbFileService
	def sdfFileService
	def matchFileService
	
	private ProcessLauncher run(command) throws CommandNotExistsException {

		log.debug command

		try{
			
			def p = new ProcessLauncher(command)
			p.launch()
			while(!p.hasFinished());

			return p;
		
		}catch(CommandNotExistsException e){
			throw e;
		}catch(Exception e){
			log.error "PharmerService exec ${e.message}"
			throw e;
		}
		
	}

	def generateDatabaseName(pharmerDb) {
		return pharmerDb.name = "db" + pharmerDb.id
	}

	public void add(molecule) throws Exception {

		log.info "PharmerService adding ${molecule}"
		
		createPharma(molecule)
		
		def pDb = new PharmerDB(fileNamePdb: molecule.fileNamePdb).save(flush: true)

		pDb.name = pdbFileService.getUploadPathFor(generateDatabaseName(pDb))

		def p = run(
		PharmerCommand.commandCreateDatabase(pDb.name, sdfFileService.get(molecule.fileNameSdf))
		)
		
		if(p.out.length()>200)
			pDb.output = p.out.substring(0, 200)
		else
			pDb.output = p.out
		
		pDb.save()

	}
	
	public void del(molecule) throws Exception {
		
		def pDb = PharmerDB.findByFileNamePdb(molecule.fileNamePdb)
		
		if(!pDb)
			return
		
		sdfFileService.delete(molecule.fileNameSdf)
		pdbFileService.delete(molecule.fileNamePdb)
		pDb.delete(flush: true)
		
	}

	public String createPharma(molecule) throws Exception {

		log.info "PharmerService creating pharma from ${molecule}"
		
		def fileNameSdf = generateFilenameSDF(molecule.fileNamePdb)
		
		def p = run(
		PharmerCommand.commandCreatePharma(pdbFileService.get(molecule.fileNamePdb), sdfFileService.get(fileNameSdf))
		)
		
		molecule.fileNameSdf = fileNameSdf
		molecule.save(flush: true)

		return p.out
	}

	public String search(molecule, output) throws Exception {

		log.info "PharmerService search with ${molecule}"
		
		def userResult = output+".sdf"
    	def userQuery = output+".json"

    	def p1 = run(
    	PharmerCommand.commandCreateQuery(pdbFileService.get(molecule.fileNamePdb), matchFileService.getUploadPathFor(userQuery))
    	)

		def p2 = run(
		PharmerCommand.commandSearch(PharmerDB.list(), matchFileService.get(userQuery), matchFileService.get(userResult))
		)
		
		log.info "PharmerService search store in ${userResult}"
		
		return userResult
	}
	
	public String generateFilenameSDF(filename) {
		return filename.replace(".pdb",".sdf")
	}
	
}

class PharmerCommand {
	
	static String commandCreateDatabase(database, input) {

		return MessageFormat.format(
		ConfigurationHolder.config.pharmer.dbcreate,
		MessageFormat.format(ConfigurationHolder.config.pharmer.dbdir, database),
		MessageFormat.format(ConfigurationHolder.config.pharmer.input, input)
		)
	}

	static String commandCreatePharma(input, output) {

		return MessageFormat.format(
		ConfigurationHolder.config.babel.convert,
		input,
		output
		)
	}
	
	static String commandCreateQuery(input, output) {

		return MessageFormat.format(
		ConfigurationHolder.config.pharmer.pharma,
		MessageFormat.format(ConfigurationHolder.config.pharmer.input, input),
		MessageFormat.format(ConfigurationHolder.config.pharmer.output, output)
		)
	}

	static String commandSearch(databases, input, output) {

		return MessageFormat.format(
		ConfigurationHolder.config.pharmer.dbsearch,
		argDatabase(databases),
		MessageFormat.format(ConfigurationHolder.config.pharmer.input, input),
		MessageFormat.format(ConfigurationHolder.config.pharmer.output, output)
		)
	}
	
	static String argDatabase(databases) {
		def dbs = ""

		databases.each {
			db ->
			dbs += MessageFormat.format(
			ConfigurationHolder.config.pharmer.dbdir,
			db.name
			) + " "  
		}

		if(StringUtils.isBlank(dbs)) {
			dbs = MessageFormat.format(ConfigurationHolder.config.pharmer.dbdir, ConfigurationHolder.config.pharmer.db0)
		}

		return dbs;
	}
}
