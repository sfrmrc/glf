package it.pharm.med

import it.pharm.med.command.exception.CommandNotExistsException
import it.pharm.med.file.MatchFile
import it.pharm.med.pharmer.PharmerService

class SearchService {

	def pharmerService
	def matchFileService
	
	def search(molecule, user) throws CommandNotExistsException {
		
		try{
		
			def output = new String(user.hashCode()+""+new Date().hashCode())
			def fileResult = pharmerService.search(molecule, output)
			
			def mf = new MatchFile(matchFileService.get(fileResult))
			
			return [matchInfo: mf.parse(), fileResult: fileResult]
		
		}catch(Exception e){
			log.error "SearchService search ${e.message}"
		}
		
	}
	
}
