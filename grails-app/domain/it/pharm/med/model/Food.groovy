package it.pharm.med.model

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder

class Food implements Serializable {

	String name

	List molecules

	static belongsTo = [subGroup: SubGroup]

	static hasMany = [molecules: FoodMolecule]

	static constraints = {
		name blank: false, unique: true
	}

	static mapping = { molecules cascade: "all-delete-orphan" }

	def String toString() {
		return name
	}

	public FoodMolecule link(molecule) {
		
		def fm = FoodMolecule.findByFoodAndMolecule(this, molecule)
		
		if(!fm) {

			addToMolecules new FoodMolecule(food: this, molecule: molecule)
						
		}
		
		return fm
	}
	
	public void unlink(molecule) {
		
		def fm = FoodMolecule.findByFoodAndMolecule(this, molecule)
		
		if(fm) {

			removeFromMolecules fm
						
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {

		if(obj == null)
			return false;

		if(!(obj instanceof Food))
			return false;

		Food that = (Food)obj;

		if(this.id==null || that.id==null)
			return new EqualsBuilder()
			.append(this.name, that.name)
			.isEquals();

		return new EqualsBuilder()
		.append(this.id, that.id)
		.isEquals();
	}
	
	public static textSearch(String text) {
		def c = Food.createCriteria()
		
		return c.list {
			ilike("name", "%${text}%")
		}
		
	}
	
}
