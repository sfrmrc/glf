package it.pharm.med.model

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder

class SubGroup implements Serializable {

	String name

	static belongsTo = [group : Group]

	static constraints = {
		name blank:false, unique:true
	}

	@Override
	public String toString() {
		return name
	}

	@Override
	public boolean equals(Object obj) {

		if(obj == null)
			return false;

		if(!(obj instanceof SubGroup))
			return false;

		SubGroup that = (SubGroup)obj;

		if(this.id==null || that.id==null)
			return new EqualsBuilder()
			.append(this.name, that.name)
			.isEquals();

		return new EqualsBuilder()
		.append(this.id, that.id)
		.isEquals();
	}
}
