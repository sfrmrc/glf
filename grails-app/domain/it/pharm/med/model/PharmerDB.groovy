package it.pharm.med.model

import java.io.Serializable;

class PharmerDB implements Serializable {

	String name
	String fileNamePdb
	String output
	
    static constraints = {
		name unique: true, nullable: true
		fileNamePdb unique: true
		output nullable: true
    }
	
}
