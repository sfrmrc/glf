package it.pharm.med.model

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

class FoodMolecule implements Serializable {

	Molecule molecule
	Food food

	Double amount
		
	boolean deleted

	static transients = [ 'deleted']
	
	static belongsTo = [food: Food]
	
	static hasOne = [molecule: Molecule]
	
	static constraints = {
		amount nullable: true
	}
	
	static mapping = {
		id composite: ['food', 'molecule']
		version false
	}
	
	@Override
	public String toString() {
		return molecule
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null)
		return false;

		if(!(obj instanceof FoodMolecule))
				return false;
										
		FoodMolecule that = (FoodMolecule)obj;
		
		return new EqualsBuilder()
				.append(this.molecule, that.molecule)
				.append(this.food, that.food)
				.isEquals();
		
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(this.molecule)
			.append(this.food)
			.toHashCode();
	}
	
}