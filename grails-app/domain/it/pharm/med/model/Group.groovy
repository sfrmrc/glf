package it.pharm.med.model

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder

class Group implements Serializable {

	String name

	static constraints = {
		name blank:false, unique: true
	}

	static mapping = { table "`group`" }

	@Override
	public String toString() {
		return name
	}

	@Override
	public boolean equals(Object obj) {

		if(obj == null)
			return false;

		if(!(obj instanceof Group))
			return false;

		Group that = (Group)obj;

		if(this.id==null || that.id==null)
			return new EqualsBuilder()
			.append(this.name, that.name)
			.isEquals();

		return new EqualsBuilder()
		.append(this.id, that.id)
		.isEquals();
	}
}
