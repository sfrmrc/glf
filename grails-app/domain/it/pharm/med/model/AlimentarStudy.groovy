package it.pharm.med.model

import org.apache.commons.lang.builder.EqualsBuilder;
import org.codehaus.groovy.classgen.AsmClassGenerator;
import org.dbxp.moduleBase.Study

class AlimentarStudy extends Study {

	String description
	
	List molecules
	List foods

	static constraints = {
		description nullable: true
	}
	
	static hasMany = [
		molecules: AlimentarStudyMolecule,
		foods: AlimentarStudyFood
	]

	static mapping = {
		molecules cascade: "all-delete-orphan"
		foods cascade: "all-delete-orphan"
	}

	/**
	 * Sets the properties of this object, based on the JSON object given by GSCF
	 * @param jsonObject	Object with study data from GSCF
	 */
	@Override
	public void setPropertiesFromGscfJson( jsonObject ) {
		super.setPropertiesFromGscfJson( jsonObject );

		if( this.name ) {
			
			if( this.molecules==null )
				this.molecules = []
				
			if( this.foods==null)
				this.foods = []
		
			Molecule.withCriteria {
				sqlRestriction "'${this.name}' like concat('%', name, '%')"
			}.each { molecule ->
			
				def asm = new AlimentarStudyMolecule()
				asm.molecule = molecule
				asm.alimentarStudy = this
		
				if(!this.molecules.contains(asm))
					this.addToMolecules asm
				
			}
			
			Food.withCriteria {
				sqlRestriction "'${this.name}' like concat('%', name, '%')"
			}.each { food ->
			
				def asf = new AlimentarStudyFood()
				asf.food = food
				asf.alimentarStudy = this
				
				if(!this.foods.contains(asf))
					this.addToFoods asf
			
			}
	
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {

		if(obj == null)
			return false;

		if(!(obj instanceof AlimentarStudy))
			return false;

		AlimentarStudy that = (AlimentarStudy)obj;

		if(this.id==null || that.id==null)
			return new EqualsBuilder()
			.append(this.name, that.name)
			.isEquals();

		return new EqualsBuilder()
		.append(this.id, that.id)
		.isEquals();
	}
	
	public static textSearch(String text) {
		def c = AlimentarStudy.createCriteria()
		
		return c.list {
			or {
				ilike("name", "%${text}%")
				ilike("description", "%${text}%")
			}
		}
		
	}
	
}
