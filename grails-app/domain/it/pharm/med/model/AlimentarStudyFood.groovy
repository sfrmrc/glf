package it.pharm.med.model

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder

class AlimentarStudyFood implements Serializable {

	AlimentarStudy alimentarStudy
	Food food
	
	boolean deleted
	static transients = [ 'deleted' ]

	static belongsTo = [alimentarStudy: AlimentarStudy]
	
	static hasOne = [food: Food]
		
	static mapping = {
		id composite: ['food', 'alimentarStudy']
		version false
	}
	
	@Override
	public String toString() {
		return food.name;
	}
	
	@Override
	public boolean equals(Object obj) {

		if(obj == null)
		return false;

		if(!(obj instanceof AlimentarStudyFood))
				return false;
										
		AlimentarStudyFood that = (AlimentarStudyFood)obj;
		
		return new EqualsBuilder()
			.append(this.food, that.food)
			.append(this.alimentarStudy, that.alimentarStudy)
			.isEquals();
		
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(this.food)
			.append(this.alimentarStudy)
			.toHashCode();
	}
	
}
