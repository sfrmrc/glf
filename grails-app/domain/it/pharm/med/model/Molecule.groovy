package it.pharm.med.model

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder

class Molecule implements Serializable {

	String name
	String fileNamePdb
	String fileNameSdf
	String alias
	String molecularWeight
	String molecularFormula

	List foods

	static hasMany = [foods: FoodMolecule]

	static constraints = {
		name blank: false, unique: true
		alias nullable: true
		fileNamePdb nullable: true
		fileNameSdf nullable: true
		molecularWeight nullable: true
		molecularFormula nullable: true
	}

	static mapping = { foods cascade: "all-delete-orphan" }

	def String toString() {
		return name
	}

	@Override
	public boolean equals(Object obj) {

		if(obj == null)
			return false;

		if(!(obj instanceof Molecule))
			return false;

		Molecule that = (Molecule)obj;

		if(this.id==null || that.id==null)
			return new EqualsBuilder()
			.append(this.name, that.name)
			.isEquals();

		return new EqualsBuilder()
		.append(this.id, that.id)
		.isEquals();
	}
	
	public static textSearch(String text) {
		def c = Molecule.createCriteria()
		
		return c.list {
			or {
				rlike("name", "%${text}%")
				rlike("alias", "%${text}%")
			}
		}
		
	}
	
}
