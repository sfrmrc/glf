package it.pharm.med.model

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder

class AlimentarStudyMolecule implements Serializable {

	AlimentarStudy alimentarStudy
	Molecule molecule
	
	boolean deleted
	static transients = [ 'deleted' ]

	static belongsTo = [alimentarStudy: AlimentarStudy]
	
	static hasOne = [molecule: Molecule]
		
	static mapping = {
		id composite: ['molecule', 'alimentarStudy']
		version false
	}
	
	static AlimentarStudyMolecule link(alimentarStudy, molecule) {
		
		def sf = AlimentarStudyMolecule.findByAlimentarStudyAndMolecule(alimentarStudy, molecule)
		
		if(!sf) {

			sf = new AlimentarStudyMolecule()
			
			sf.molecule = molecule
			sf.alimentarStudy = alimentarStudy
			
			sf.save()
						
		}
		
		return sf
	}
	
	static void unlink(alimentarStudy, molecule) {
		
		def sf = AlimentarStudyMolecule.findByAlimentarStudyAndMolecule(alimentarStudy, molecule)
		
		if(sf) {

			sf.delete()
						
		}
		
	}
	
	@Override
	public String toString() {
		return molecule.name;
	}
	
	@Override
	public boolean equals(Object obj) {

		if(obj == null)
		return false;

		if(!(obj instanceof AlimentarStudyMolecule))
				return false;
										
		AlimentarStudyMolecule that = (AlimentarStudyMolecule)obj;
		
		return new EqualsBuilder()
				.append(this.molecule, that.molecule)
				.append(this.alimentarStudy, that.alimentarStudy)
				.isEquals();
		
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(this.molecule)
			.append(this.alimentarStudy)
			.toHashCode();
	}
	
}
