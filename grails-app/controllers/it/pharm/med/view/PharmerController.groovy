package it.pharm.med.view

import grails.converters.JSON
import it.pharm.med.command.exception.CommandNotExistsException;

import it.pharm.med.model.PharmerDB
import it.pharm.med.model.Molecule
import it.pharm.med.SearchService

class PharmerController {

	def searchService
	
    def index() { }
	
	def search = {
		
		try{
		
			def moleculeInstance = Molecule.findByName(params.molecule.name)
			def searchResult = searchService.search(moleculeInstance, session.user)
			render searchResult as JSON
		
		}catch(CommandNotExistsException e){
			def message = message(code: 'default.command.not.found.message', args: [message(code: 'pharmer.label', default: 'Pharmer'), e.getMessage()])
			render message as JSON
		}catch(Exception e){
			def message = message(code: 'default.exception.generic', args: [message(code: 'pharmer.label', default: 'Pharmer'), e.getMessage()])
			render message as JSON
		}
	
	}
	
	def config = {
	
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[pharmerDbInstanceList: PharmerDB.list(params), pharmerDbInstanceTotal: PharmerDB.count()]
		
	}
	
}
