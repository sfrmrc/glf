package it.pharm.med.view

import grails.converters.JSON
import org.dbxp.moduleBase.RefreshUserInformation;
import it.pharm.med.model.AlimentarStudy

class HomeController {

    def index() { }
	
	def ajaxStudySearch = {
		
		def query = params.query;
		def result	= [ total: 0, data: [] ]
		
		AlimentarStudy.textSearch(query).each { study ->
			result.data << [
				link		: createLink(controller:'alimentarStudy', action:'show', id:study.id),
			    name		: "${study.name}",
				category	: 'Alimentar Study'
			]
		}
		
		result.total = result.data.size()
		
		if (!result.total) {
			result.data << [
				link	: '',
				name	: "no results",
				category: ""
			]
		}
		
		response.contentType = "application/json"
		if (params.callback) {
			render "${params.callback}(${result as JSON})"
		} else {
			render result as JSON
		}
		
	}
	
}
