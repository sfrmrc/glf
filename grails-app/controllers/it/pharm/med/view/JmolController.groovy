package it.pharm.med.view

import it.pharm.med.model.Molecule

class JmolController {

	def index = {}
	
	def show = {
		
		def moleculeInstance = Molecule.get(params.id)
		
		[moleculeInstance: moleculeInstance]
	}
		
}
