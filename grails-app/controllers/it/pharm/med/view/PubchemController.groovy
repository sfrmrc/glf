package it.pharm.med.view

import grails.converters.JSON
import it.pharm.med.model.Molecule

import it.pharm.med.pubchem.PubchemService

class PubchemController {

	def pubchemService
	
    def index() { }
	
	def search = {
		
		try{
			
			def moleculeInstance = new Molecule()
			moleculeInstance.properties = params
			
			if(!moleculeInstance)
				throw new IllegalArgumentException("Molecule not found with id: $params.id")
							
			render pubchemService.searchMolecule(moleculeInstance) as JSON
			
		}catch(Exception e){
			log.error e.message
		}
		
	}
	
	def download = {
		
		redirect (url: pubchemService.download(params.id))
	}
	
}
