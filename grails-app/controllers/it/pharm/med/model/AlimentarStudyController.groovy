package it.pharm.med.model

import org.dbxp.moduleBase.*;
import org.springframework.dao.DataIntegrityViolationException

class AlimentarStudyController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']
	
	def synchronizationService
	def gscfService

	@RefreshUserInformation
    def index = {
        redirect action: 'list', params: params
    }
	
	@RefreshUserInformation
	def synchronize = {
		try {
			synchronizationService.initSynchronization( session.sessionToken, session.user ) ;
			synchronizationService.fullSynchronization();
		} catch( Exception e ) {
			e.printStackTrace();
			flash.error = "An error occurred while synchronizing: " + e.getMessage() + "; Please try again."
		}
		
		redirect(action: "list", params: params)
	}
	
    @RefreshUserInformation
    def list = {

		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[alimentarStudyInstanceList: AlimentarStudy.list(params), alimentarStudyInstanceTotal: AlimentarStudy.count()]
		
    }

	@RefreshUserInformation
    def show = {
		
        def alimentarStudyInstance = AlimentarStudy.get(params.id)
		
        if (!alimentarStudyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'alimentarStudy.label', default: 'AlimentarStudy'), params.id])
            redirect action: 'list'
            return
        }
		
        [alimentarStudyInstance: alimentarStudyInstance, gscfLink: gscfService.urlViewStudy(alimentarStudyInstance.studyToken)]
		
    }

}
