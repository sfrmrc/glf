package it.pharm.med.model

import it.pharm.med.command.exception.CommandNotExistsException;
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MoleculeController extends DialogController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

	def pdbFileService
	def pharmerService
	
    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [moleculeInstanceList: Molecule.list(params), moleculeInstanceTotal: Molecule.count()]
    }
	
	def search() {
	}
	
    def create() {
		switch (request.method) {
		case 'GET':
        	[moleculeInstance: new Molecule(params)]
			break
		case 'POST':
	        def moleculeInstance = new Molecule(params)
			
	        if (!moleculeInstance.save(flush: true)) {
	            render view: 'create', model: [moleculeInstance: moleculeInstance]
	            return
	        }

			def file = request.getFile('pdbFile')
			if(!file.empty)
				modifyPdbFile(moleculeInstance,file)
			
			flash.message = message(code: 'default.created.message', args: [message(code: 'molecule.label', default: 'Molecule'), moleculeInstance.id])
	        redirect action: 'show', id: moleculeInstance.id, params: extraparams()
			break
		}
    }

    def show() {
        def moleculeInstance = Molecule.get(params.id)
        if (!moleculeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
            redirect action: 'list'
            return
        }

        [moleculeInstance: moleculeInstance, params: extraparams()]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def moleculeInstance = Molecule.get(params.id)
	        if (!moleculeInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [moleculeInstance: moleculeInstance]
			break
		case 'POST':
	        def moleculeInstance = Molecule.get(params.id)
			
			def file = request.getFile('pdbFile')
			if(!file.empty)
				modifyPdbFile(moleculeInstance, file)
			
	        if (!moleculeInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (moleculeInstance.version > version) {
	                moleculeInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'molecule.label', default: 'Molecule')] as Object[],
	                          "Another user has updated this Molecule while you were editing")
	                render view: 'edit', model: [moleculeInstance: moleculeInstance]
	                return
	            }
	        }

	        moleculeInstance.properties = params

	        if (!moleculeInstance.save(flush: true)) {
	            render view: 'edit', model: [moleculeInstance: moleculeInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'molecule.label', default: 'Molecule'), moleculeInstance.id])
	        redirect action: 'show', id: moleculeInstance.id, params: extraparams()
			break
		}
    }

    def delete() {
        def moleculeInstance = Molecule.get(params.id)
        if (!moleculeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
            redirect action: 'list'
            return
        }

        try {

			def moleculeTmp = new Molecule()
			moleculeTmp.fileNamePdb = moleculeInstance.fileNamePdb
			moleculeTmp.fileNameSdf = moleculeInstance.fileNameSdf
			
			moleculeInstance.delete(flush: true)
			pharmerService.del(moleculeTmp)
			
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'molecule.label', default: 'Molecule'), params.id])
            redirect action: 'show', id: params.id
        }
    }
	
	String modifyPdbFile(moleculeInstance, file) {
		
		try{
			
			def newfilename = pdbFileService.moveFileToUploadDir( file, file.getOriginalFilename() );
			moleculeInstance.fileNamePdb = newfilename
			
			pharmerService.add(moleculeInstance)
		
		}catch(CommandNotExistsException e){
			flash.pharmerMessage = message(code: 'default.command.not.found.message', args: [message(code: 'pharmer.label', default: 'Pharmer'), e.getMessage()])
		}catch(Exception e){
			flash.pharmerMessage = message(code: 'default.command.generic.error', args: [message(code: 'pharmer.label', default: 'Pharmer'), newfilename])
		}
		
	}
	
	def ajaxSearch = {
		
		def criteria = Molecule.createCriteria()
		
		def results = criteria.list {
			projections {
				property 'name'
			}
			ilike 'name', params.term + '%'
		}
		
		render text: results as JSON, contentType: 'application/json'
		
	}
	
}
