package it.pharm.med.model

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class FoodController extends DialogController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [foodInstanceList: Food.list(params), foodInstanceTotal: Food.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[foodInstance: new Food(params)]
			break
		case 'POST':
	        def foodInstance = new Food(params)
	        if (!foodInstance.save(flush: true)) {
	            render view: 'create', model: [foodInstance: foodInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'food.label', default: 'Food'), foodInstance.id])
	        redirect action: 'show', id: foodInstance.id, params: extraparams()
			break
		}
    }

    def show() {
        def foodInstance = Food.get(params.id)
        if (!foodInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'food.label', default: 'Food'), params.id])
            redirect action: 'list'
            return
        }

        [foodInstance: foodInstance, params: extraparams()]
    }

    def edit() {
		render view: 'edit', model: [foodInstance: Food.get(params.id)]
    }
	
	def update() {
		
		def foodInstance = Food.get(params.id)
		
		foodInstance.properties = params
		
		foodInstance.molecules.removeAll { it.deleted }
		
		foodInstance.molecules.each { foodMolecule -> 
			foodMolecule.molecule = Molecule.findByName(foodMolecule.molecule.name)
		}
		
		foodInstance.save(flush: true)
		
		redirect action: 'show', params: extraparams()
		
	}

    def delete() {
        def foodInstance = Food.get(params.id)
        if (!foodInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'food.label', default: 'Food'), params.id])
            redirect action: 'list'
            return
        }

        try {
            foodInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'food.label', default: 'Food'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'food.label', default: 'Food'), params.id])
            redirect action: 'show', id: params.id
        }
    }
	
	def ajaxSearch = {
		
		def criteria = Food.createCriteria()
		
		def results = criteria.list {
			projections {
				property 'name'
			}
			ilike 'name', params.term + '%'
		}
		
		render text: results as JSON, contentType: 'application/json'
	
	}
	
}
