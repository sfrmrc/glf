package it.pharm.med.model

import org.springframework.dao.DataIntegrityViolationException

class SubGroupController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [subGroupInstanceList: SubGroup.list(params), subGroupInstanceTotal: SubGroup.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[subGroupInstance: new SubGroup(params)]
			break
		case 'POST':
	        def subGroupInstance = new SubGroup(params)
	        if (!subGroupInstance.save(flush: true)) {
	            render view: 'create', model: [subGroupInstance: subGroupInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), subGroupInstance.id])
	        redirect action: 'show', id: subGroupInstance.id
			break
		}
    }

    def show() {
        def subGroupInstance = SubGroup.get(params.id)
        if (!subGroupInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
            redirect action: 'list'
            return
        }

        [subGroupInstance: subGroupInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def subGroupInstance = SubGroup.get(params.id)
	        if (!subGroupInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [subGroupInstance: subGroupInstance]
			break
		case 'POST':
	        def subGroupInstance = SubGroup.get(params.id)
	        if (!subGroupInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (subGroupInstance.version > version) {
	                subGroupInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'subGroup.label', default: 'SubGroup')] as Object[],
	                          "Another user has updated this SubGroup while you were editing")
	                render view: 'edit', model: [subGroupInstance: subGroupInstance]
	                return
	            }
	        }

	        subGroupInstance.properties = params

	        if (!subGroupInstance.save(flush: true)) {
	            render view: 'edit', model: [subGroupInstance: subGroupInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), subGroupInstance.id])
	        redirect action: 'show', id: subGroupInstance.id
			break
		}
    }

    def delete() {
        def subGroupInstance = SubGroup.get(params.id)
        if (!subGroupInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
            redirect action: 'list'
            return
        }

        try {
            subGroupInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subGroup.label', default: 'SubGroup'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
