$(document).ready(function() {

	$('#spinner').ajaxStart(function() {
		$(this).fadeIn();
	}).ajaxStop(function() {
		$(this).fadeOut();
	});
	
	$(document).tooltip({
		selector: "a[rel=tooltip]"
	});
	
	$('select').chosen();
	
});

function modal(element) {
	
	element.click(function(e) {
		e.preventDefault();

		var uri = $(e.target).attr('href');
		$('<iframe id="dialog" class="modal fade" data-replace="true" width="800" height="600" frameborder="0" src="' + uri + '" />').modal();
	
	});
	
}

function attachTableMetadata(tableId, linkId, onAdd, onDel) {
	
    $.metadata.setType("attr", "data");

    $("#"+tableId).writetable({
        autoAddRow: false,
        rowAdded: function( event, row ) {
            console.debug("in the rowAdded callback");
            
            var button = $('<button class="btn btn-small delete-row-button" type="button" >'+
					'<i class="icon-minus"></i>'+
				'</button>');
            
            button.on("click", function(event) {
                console.debug("in the delete-row-button click handler");
                var target = $(event.target);
                var row = target.closest('tr');
                $("#"+tableId).writetable('removeRow', event, row);
            });
            
            $(row).children("td").last().append(button);
            
            if(typeof onAdd === 'function')
            	onAdd(row);
        },
        rowSelected: function(event, row) {
            console.debug("in the rowSelected callback");
            /*
             * FIXME:
             */
            if(typeof onAdd === 'function')
            	onAdd(row);
        },
        rowRemoved: function(event, row) {
            console.debug("in the rowRemoved callback handler");
            var rowId =  $(row).attr('rowId');
            $(row).parent().append("<input type='hidden' name='"+tableId+"[" + rowId + "].deleted' value='true' />");
            if(typeof onDel === 'function' )
            	onDel(row);
        }
    });

    $("#"+linkId).click(function() {
        console.debug("in the click handler");
        $("#"+tableId).writetable("addRow");
        return false;
    });
    
    $('button.delete-row-button').on("click", function(event) {
        console.debug("in the delete-row-button click handler");
        var target = $(event.target);
        var row = target.closest('tr');
        $("#"+tableId).writetable('removeRow', event, row);
    });

}
