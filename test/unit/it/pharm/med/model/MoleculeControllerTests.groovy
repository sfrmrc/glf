package it.pharm.med.model



import org.junit.*
import grails.test.mixin.*

@TestFor(MoleculeController)
@Mock(Molecule)
class MoleculeControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/molecule/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.moleculeInstanceList.size() == 0
        assert model.moleculeInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.moleculeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.moleculeInstance != null
        assert view == '/molecule/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/molecule/show/1'
        assert controller.flash.message != null
        assert Molecule.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/molecule/list'

        populateValidParams(params)
        def molecule = new Molecule(params)

        assert molecule.save() != null

        params.id = molecule.id

        def model = controller.show()

        assert model.moleculeInstance == molecule
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/molecule/list'

        populateValidParams(params)
        def molecule = new Molecule(params)

        assert molecule.save() != null

        params.id = molecule.id

        def model = controller.edit()

        assert model.moleculeInstance == molecule
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/molecule/list'

        response.reset()

        populateValidParams(params)
        def molecule = new Molecule(params)

        assert molecule.save() != null

        // test invalid parameters in update
        params.id = molecule.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/molecule/edit"
        assert model.moleculeInstance != null

        molecule.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/molecule/show/$molecule.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        molecule.clearErrors()

        populateValidParams(params)
        params.id = molecule.id
        params.version = -1
        controller.update()

        assert view == "/molecule/edit"
        assert model.moleculeInstance != null
        assert model.moleculeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/molecule/list'

        response.reset()

        populateValidParams(params)
        def molecule = new Molecule(params)

        assert molecule.save() != null
        assert Molecule.count() == 1

        params.id = molecule.id

        controller.delete()

        assert Molecule.count() == 0
        assert Molecule.get(molecule.id) == null
        assert response.redirectedUrl == '/molecule/list'
    }
}
