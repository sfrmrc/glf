package it.pharm.med.model



import org.junit.*
import grails.test.mixin.*

@TestFor(SubGroupController)
@Mock(SubGroup)
class SubGroupControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/subGroup/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.subGroupInstanceList.size() == 0
        assert model.subGroupInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.subGroupInstance != null
    }

    void testSave() {
        controller.save()

        assert model.subGroupInstance != null
        assert view == '/subGroup/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/subGroup/show/1'
        assert controller.flash.message != null
        assert SubGroup.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/subGroup/list'

        populateValidParams(params)
        def subGroup = new SubGroup(params)

        assert subGroup.save() != null

        params.id = subGroup.id

        def model = controller.show()

        assert model.subGroupInstance == subGroup
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/subGroup/list'

        populateValidParams(params)
        def subGroup = new SubGroup(params)

        assert subGroup.save() != null

        params.id = subGroup.id

        def model = controller.edit()

        assert model.subGroupInstance == subGroup
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/subGroup/list'

        response.reset()

        populateValidParams(params)
        def subGroup = new SubGroup(params)

        assert subGroup.save() != null

        // test invalid parameters in update
        params.id = subGroup.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/subGroup/edit"
        assert model.subGroupInstance != null

        subGroup.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/subGroup/show/$subGroup.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        subGroup.clearErrors()

        populateValidParams(params)
        params.id = subGroup.id
        params.version = -1
        controller.update()

        assert view == "/subGroup/edit"
        assert model.subGroupInstance != null
        assert model.subGroupInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/subGroup/list'

        response.reset()

        populateValidParams(params)
        def subGroup = new SubGroup(params)

        assert subGroup.save() != null
        assert SubGroup.count() == 1

        params.id = subGroup.id

        controller.delete()

        assert SubGroup.count() == 0
        assert SubGroup.get(subGroup.id) == null
        assert response.redirectedUrl == '/subGroup/list'
    }
}
