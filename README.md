# GLF Module 4 Generic Study Capture Framework

### Assumptions
The tutorial is based on a number of assumptions:
* you have gscf instance running or accessible

### Properties
You have to define your own properties as
* gscf.baseURL= Base URL of GSCF instance
* grails.serverURL= Server URL of the module
* module.consumerId= Consumer ID of this module that is used in communication with GSCF
* pharmer.files.dbs= Base path of molecule databases, es ${HOME}/.dbxp/dbs/
* pharmer.files.pdb= Base path of PDB (JMol) files, you may want set this directory inside your webapp ${HOME}/.dbxp/pdb/
* pharmer.files.match= Base path of Pharmer search result, es ${HOME}/.dbxp/match/ (as PDBs)
* pharmer.files.sdf= Base path of OpenBable files, es ${HOME}/.dbxp/sdf/
